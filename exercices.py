"""
Exercices.

Implémentez les méthodes ci-dessous.
Pour executer vos tests il vous faudra utiliser pytest
"""
import math

def htc_to_ttc(htc_cost: float, discount_rate: float = 0) -> float:
    """
    Exercice 1 :
    Calcule le coût TTC d'un produit.
    discount_rate : taux de réduction compris entre 0 et 1
    Taux de taxes : 20.6 %
    Retourne un float arrondi à deux décimales
    """
    if discount_rate < 0 or discount_rate > 1:
        raise Exception
    ttc_cost = round(htc_cost * 1.206 * (1 - discount_rate), 2)
    return ttc_cost


def divisors(value: int = 0):
    """
    Exercice 2 :
    A partir d'un nombre donné,
    retourne ses diviseurs (sans répétition)
    s’il y en a, ou « PREMIER » s’il n’y en a pas.
    """
    divisors_set = []
    initial_value = value
    if value == 0:
        return "All integer numbers different from 0 are divisors of 0"
    if value == 1:
        return "ONE"
    if value < 0:
        return "The value should be positive integer"
    # if the number is 2
    if value == 2:
        return "PREMIER"
    # number of two's that divide n
    while value % 2 == 0:
        divisors_set.append(2)
        value = value / 2
    # n must be odd at this point
    # so a skip of 2 ( i = i + 2) can be used
    for i in range(3, int(math.sqrt(value)) + 1, 2):
        # while i divides n , print i ad divide n
        while value % i == 0:
            divisors_set.append(i)
            value = value / i
    if 2 < value < initial_value:
        divisors_set.append(value)
    if len(divisors_set) == 0:
        return "PREMIER"
    return set(divisors_set)
